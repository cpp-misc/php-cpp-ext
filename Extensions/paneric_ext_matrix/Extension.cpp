#include <phpcpp.h>
#include "PanericExtMatrix.h"

extern "C"
{
    PHPCPP_EXPORT void *get_module()
    {
        static Php::Extension extension("paneric_ext_matrix", "1.0");

        Php::Class<PanericExt::Matrix> panericExtMatrix("PanericExt\\Matrix");

        panericExtMatrix.method<&PanericExt::Matrix::inverse>(
            "inverse", 
            {
                Php::ByVal("a", Php::Type::Array),
                Php::ByVal("l", Php::Type::Numeric)
            }
        );

        panericExtMatrix.method<&PanericExt::Matrix::prepareIdentityMatrix>(
            "prepareIdentityMatrix", 
            {
                Php::ByVal("lr", Php::Type::Numeric),
                Php::ByVal("lc", Php::Type::Numeric)
            }
        );

        panericExtMatrix.method<&PanericExt::Matrix::multiply>(
            "multiply", 
            {
                Php::ByVal("a1", Php::Type::Array),
                Php::ByVal("lr1", Php::Type::Numeric),
                Php::ByVal("lc1", Php::Type::Numeric),
                Php::ByVal("a2", Php::Type::Array),
                Php::ByVal("lr2", Php::Type::Numeric),
                Php::ByVal("lc2", Php::Type::Numeric)              
            }
        );

        panericExtMatrix.method<&PanericExt::Matrix::add>(
            "add", 
            {
                Php::ByVal("a1", Php::Type::Array),
                Php::ByVal("a2", Php::Type::Array),                
                Php::ByVal("lr", Php::Type::Numeric),
                Php::ByVal("lc", Php::Type::Numeric),
                Php::ByVal("same", Php::Type::Bool)
            }
        );

        panericExtMatrix.method<&PanericExt::Matrix::transpose>(
            "transpose", 
            {
                Php::ByVal("a", Php::Type::Array),              
                Php::ByVal("lr", Php::Type::Numeric),
                Php::ByVal("lc", Php::Type::Numeric)            
            }
        );

        panericExtMatrix.method<&PanericExt::Matrix::calculateDeterminant>(
            "calculateDeterminant",
            {
                Php::ByVal("a", Php::Type::Array),              
                Php::ByVal("l", Php::Type::Numeric)        
            }
        );         
        
        panericExtMatrix.method<&PanericExt::Matrix::prepareComplementMatrix>(
            "prepareComplementMatrix",
            {
                Php::ByVal("a", Php::Type::Array),              
                Php::ByVal("l", Php::Type::Numeric)        
            }
        );

        panericExtMatrix.method<&PanericExt::Matrix::multiplyByDouble>(
            "multiplyByDouble",
            {
                Php::ByVal("a", Php::Type::Array),              
                Php::ByVal("lr", Php::Type::Numeric),
                Php::ByVal("lc", Php::Type::Numeric),
                Php::ByVal("d", Php::Type::Float)                                
            }
        );

        extension.add(std::move(panericExtMatrix));

        return extension;
    }
}
