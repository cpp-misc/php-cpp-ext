#include "Inverser.h"

PanericExt::Inverser::Inverser()
{
    this->math = *new Math();
}

Php::Value PanericExt::Inverser::inverse(Php::Parameters &params)
{
    Php::Value a = (Php::Value)params[0];
    int l = (int)params[1];

    Php::Value ai;
    
    double detA = this->math.calculateDeterminant(a, l);

    if (detA == 0.0) {
        return ai;
    }

    Php::Value ac = this->math.prepareComplementMatrix(a, l);

    return this->math.multiplyByDouble(ac, l, l, 1/detA);
}


Php::Value PanericExt::Inverser::prepareIdentityMatrix(Php::Parameters &params)
{
    return this->math.prepareIdentityMatrix(params);
}


Php::Value PanericExt::Inverser::multiply(Php::Parameters &params)
{
    Php::Value a1 = (Php::Value)params[0];
    int lr1 = (int)params[1];
    int lc1 = (int)params[2];
    Php::Value a2 = (Php::Value)params[3];
    int lr2 = (int)params[4];
    int lc2 = (int)params[5];

    return this->math.multiply(a1, lr1, lc1, a2, lr2, lc2);
}

Php::Value PanericExt::Inverser::add(Php::Parameters &params)
{
    Php::Value a1 = (Php::Value)params[0];
    Php::Value a2 = (Php::Value)params[1];
    int lr = (int)params[2];
    int lc = (int)params[3];
    bool same = (int)params[4];

    return this->math.add(a1, a2, lr, lc, same);
}

Php::Value PanericExt::Inverser::transpose(Php::Parameters &params)
{
    Php::Value a = (Php::Value)params[0];
    int lr = (int)params[1];
    int lc = (int)params[2];

    return this->math.transpose(a, lr, lc);
}

Php::Value PanericExt::Inverser::calculateDeterminant(Php::Parameters &params)
{
    Php::Value a = (Php::Value)params[0];
    int l = (int)params[1];

    return this->math.calculateDeterminant(a, l);
}

Php::Value PanericExt::Inverser::prepareComplementMatrix(Php::Parameters &params)
{
    Php::Value a = (Php::Value)params[0];
    int l = (int)params[1];

    return this->math.prepareComplementMatrix(a, l);
};

Php::Value PanericExt::Inverser::multiplyByDouble(Php::Parameters &params)
{
    Php::Value a = (Php::Value)params[0];
    int lr = (int)params[1];
    int lc = (int)params[2];
    double d = (int)params[3];

    return this->math.multiplyByDouble(a, lr, lc, d);
}
