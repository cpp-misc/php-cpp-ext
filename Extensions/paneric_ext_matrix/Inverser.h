#pragma once

#include <phpcpp.h>
#include "Math.h"

namespace PanericExt
{
    class Inverser
    {
    private:
        Math math;    
    public:
        Inverser();
        virtual ~Inverser() = default;

        Php::Value inverse(Php::Parameters &params);

        Php::Value prepareIdentityMatrix(Php::Parameters &params);

        Php::Value multiply(Php::Parameters &params);

        Php::Value add(Php::Parameters &params);

        Php::Value transpose(Php::Parameters &params);

        Php::Value calculateDeterminant(Php::Parameters &params);

        Php::Value prepareComplementMatrix(Php::Parameters &params);

        Php::Value multiplyByDouble(Php::Parameters &params);
    };
}
