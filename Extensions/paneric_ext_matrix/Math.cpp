#include <vector>
#include "Math.h"

PanericExt::Math::Math()
{
    this->utils = *new Utils();
}

Php::Value PanericExt::Math::prepareIdentityMatrix(Php::Parameters &params)
{
    return this->utils.prepareIdentityMatrix(params);
}

Php::Value PanericExt::Math::multiply(Php::Value a1, int lr1, int lc1, Php::Value a2, int lr2, int lc2)
{
    Php::Value a = this->utils.allocateDouble2DArray(lr1, lc2);

    for (int ir = 0; ir < lr1; ++ir) {
        for (int jc = 0; jc < lc2; ++jc) {
            for (int k = 0; k < lc1; ++k) {
                a[ir][jc] += double(a1[ir][k]) * double(a2[k][jc]);
            }
        }
    }

    return a;
}

Php::Value PanericExt::Math::add(Php::Value a1, Php::Value a2, int lr, int lc, bool same)
{
    Php::Value a;

    if (!same) {
        a = this->utils.allocateDouble2DArray(lr, lc);
    }

    for (int ir = 0; ir < lr; ++ir) {
        for (int jc = 0; jc < lc; ++jc) {
            if (!same) {
                a[ir][jc] = double(a1[ir][jc]) + double(a2[ir][jc]);
            } else {
                a1[ir][jc] += double(a2[ir][jc]);
            }
        }
    }

    if (same) {
        delete[] a;
    }

    return a1;
}

Php::Value PanericExt::Math::transpose(Php::Value a, int lr, int lc)
{
    Php::Value b = this->utils.allocateDouble2DArray(lc, lr);

    for (int ir = 0; ir < lr; ++ir) {
        for (int jc = 0; jc < lc; ++jc) {
            b[jc][ir] = double(a[ir][jc]);
        }
    }

    Php::Value *pA = &a;

    this->utils.deallocateDouble2DArray(pA, lr);

    return b;
}

Php::Value PanericExt::Math::calculateDeterminant(Php::Value a, int l)
{
    int d = 0;

    if (l == 1) {
        return a[0][0];
    }

    if (l == 2) {
        return (double(a[0][0]) * double(a[1][1])) - (double(a[0][1]) * double(a[1][0]));
    }

    Php::Value t = this->utils.allocateDouble2DArray(l, l); 
    int sign = 1;
    
    for (int i = 0; i < l; i++) {
        t = this->prepareSubMatrix(a, t, 0, i, l);

        d += sign * double(a[0][i]) * double(this->calculateDeterminant(t, l - 1));
        sign = -sign;
    }
   
    return d;
}

Php::Value PanericExt::Math::prepareComplementMatrix(Php::Value a, int l)
{
    Php::Value ac = this->utils.allocateDouble2DArray(l, l);
    
    Php::Value mr;

    for (int ir = 0; ir < l; ir++) {
        for (int jc = 0; jc < l; jc++) {
            mr = this->reduceMatrix(a, l, ir, jc);
            ac[ir][jc] = this->calculateDeterminant(mr, l - 1);
        }
    }

    return ac;
};

Php::Value PanericExt::Math::multiplyByDouble(Php::Value a, int lr, int lc, double d)
{
    for (int ir = 0; ir < lr; ir++) {
        for (int jc = 0; jc < lc; jc++) {
                a[ir][jc] *= d;
        }
    }

    return a;
}


Php::Value PanericExt::Math::prepareSubMatrix(Php::Value a, Php::Value t, int p, int q, int l)
{
    int i = 0, j = 0;

    for (int ir = 0; ir < l; ir++) {
        for (int jc = 0; jc < l; jc++) {
            if (ir != p && jc != q) {
                t[i][j++] = a[ir][jc];

                if (j == l - 1) {
                    j = 0;
                    i++;
                }
            }
        }
    }

    return t;
}

Php::Value PanericExt::Math::reduceMatrix(Php::Value a, int l, int i, int j)
{
    Php::Value r = this->utils.allocateDouble2DArray(l - 1, l - 1);

    for (int ir = 0; ir < l; ir++) {
        for (int jc = 0; jc < l; jc++) {
            if (ir < i && jc < j) {
                r[ir][jc] = a[ir][jc];
            }
            if (ir < i && jc > j) {
                r[ir][jc - 1] = a[ir][jc];
            }
            if (ir > i && jc < j) {
                r[ir - 1][jc] = a[ir][jc];
            }
            if (ir > i && jc > j) {
                r[ir - 1][jc - 1] = a[ir][jc];
            }
        }
    }

    return r;
}
