#pragma once

#include <phpcpp.h>
#include "Utils.h"

namespace PanericExt
{
    class Math
    {
    public:
        Math();
        virtual ~Math() = default;

        Php::Value prepareIdentityMatrix(Php::Parameters &params);

        Php::Value multiply(Php::Value a1, int lr1, int lc1, Php::Value a2, int lr2, int lc2);

        Php::Value add(Php::Value a1, Php::Value a2, int lr, int lc, bool same);

        Php::Value transpose(Php::Value a, int lr, int lc);

        Php::Value calculateDeterminant(Php::Value a, int l);

        Php::Value prepareComplementMatrix(Php::Value a, int l);

        Php::Value multiplyByDouble(Php::Value a, int lr, int lc, double d);

    private:
        Utils utils;

        Php::Value prepareSubMatrix(Php::Value a, Php::Value t, int p, int q, int l);

        Php::Value reduceMatrix(Php::Value a, int l, int i, int j);
    };
}
