#include "PanericExtMatrix.h"
#include "Inverser.h"

PanericExt::Matrix::Matrix()
{
    this->inverser = *new Inverser();
}

Php::Value PanericExt::Matrix::inverse(Php::Parameters &params)
{
    return this->inverser.inverse(params);
}

Php::Value PanericExt::Matrix::prepareIdentityMatrix(Php::Parameters &params)
{
    return this->inverser.prepareIdentityMatrix(params);
}

Php::Value PanericExt::Matrix::multiply(Php::Parameters &params)
{
    return this->inverser.multiply(params);
}

Php::Value PanericExt::Matrix::add(Php::Parameters &params)
{
    return this->inverser.add(params);
}

Php::Value PanericExt::Matrix::transpose(Php::Parameters &params)
{
    return this->inverser.transpose(params);
}

Php::Value PanericExt::Matrix::calculateDeterminant(Php::Parameters &params)
{
    return this->inverser.calculateDeterminant(params);
}

Php::Value PanericExt::Matrix::prepareComplementMatrix(Php::Parameters &params)
{
    return this->inverser.prepareComplementMatrix(params);
};

Php::Value PanericExt::Matrix::multiplyByDouble(Php::Parameters &params)
{
    return this->inverser.multiplyByDouble(params);
}
