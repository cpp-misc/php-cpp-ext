#pragma once

#include <phpcpp.h>
#include "Inverser.h"

namespace PanericExt
{
    class Matrix : public Php::Base
    {      
    private:
        Inverser inverser;
    public:
        Matrix();
        virtual ~Matrix() = default;

        Php::Value inverse(Php::Parameters &params);

        Php::Value prepareIdentityMatrix(Php::Parameters &params);

        Php::Value multiply(Php::Parameters &params);

        Php::Value add(Php::Parameters &params);

        Php::Value transpose(Php::Parameters &params);

        Php::Value calculateDeterminant(Php::Parameters &params);

        Php::Value prepareComplementMatrix(Php::Parameters &params);

        Php::Value multiplyByDouble(Php::Parameters &params);
    };
}
