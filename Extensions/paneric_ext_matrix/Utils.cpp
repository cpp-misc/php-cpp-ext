#include "Utils.h"

Php::Value PanericExt::Utils::prepareIdentityMatrix(Php::Parameters &params)
{
    int lr = (int)params[0];
    int lc = (int)params[1];

    Php::Value a = new Php::Value;

    for (int ir = 0; ir < lr; ir++)
    {
        a[ir] = new Php::Value;
    }

    for (int ir = 0; ir < lr; ir++)
    {
        for (int jc = 0; jc < lc; jc++)
        {
            if (ir == jc)
            {
                a[ir][jc] = 1.0;
            }
            else
            {
                a[ir][jc] = 0.0;
            }
        }
    }

    return a;
}

Php::Value PanericExt::Utils::allocateDouble2DArray(int lr, int lc)
{
    Php::Value a = new Php::Value;

    for (int ir = 0; ir < lr; ir++) {
        a[ir] = new Php::Value;
    }

    for (int ir = 0; ir < lr; ir++) {
        for (int jc = 0; jc < lc; jc++) {
            a[ir][jc] = 0.0;
        }
    }

    return a;
}

Php::Value PanericExt::Utils::allocate3DArray(int lw, int lc, int ld)
{   
    Php::Value a = new Php::Value;

    for (int ir = 0; ir < lw; ir++) {
        a[ir] = new Php::Value;

        for (int jc = 0; jc < lc; jc++) {
            a[ir][jc] = new Php::Value;
        }
    }

    for (int ir = 0; ir < lw; ir++) {
        for (int jc = 0; jc < lc; jc++) {
            for (int kd = 0; kd < ld; kd++) {
                a[ir][jc][kd] = 0.0;
            }
        }
    }

    return a;
}

void PanericExt::Utils::deallocateDouble2DArray(Php::Value *a, int lr)
{
    for (int ir = 0; ir < lr; ir++) {
        delete[] a[ir];
    }

    delete[] a;
}

void PanericExt::Utils::deallocateDouble3DArray(Php::Value *a, int lr, int lc)
{
    for (int ir = 0; ir < lr; ir++) {
        for (int jc = 0; jc < lc; jc++) {
            delete[] a[ir][jc];
        }

        delete[] a[ir];
    }

    delete[] a;
}
