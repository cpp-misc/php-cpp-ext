#pragma once

#include <phpcpp.h>

namespace PanericExt
{
    class Utils
    {  
    public: 
        Utils() = default;
        virtual ~Utils() = default;

        Php::Value prepareIdentityMatrix(Php::Parameters &params);

        Php::Value allocateDouble2DArray(int lr, int lc);

        Php::Value allocate3DArray(int lw, int lc, int ld);

        void deallocateDouble2DArray(Php::Value *a, int lr);

        void deallocateDouble3DArray(Php::Value *a, int lr, int lc);
    };
}
