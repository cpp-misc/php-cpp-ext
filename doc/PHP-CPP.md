## [Installing GCC the C compiler on Ubuntu 20.04 step by step instructions](https://linuxconfig.org/how-to-switch-between-multiple-gcc-and-g-compiler-versions-on-ubuntu-20-04-lts-focal-fossa)

### 1. Install multiple C and C++ compiler versions: 
```shell
$ sudo apt install build-essential
$ sudo apt -y install gcc-11 g++-11
```

### 2. Use the update-alternatives tool to create list of multiple GCC and G++ compiler alternatives:
```shell
$ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-11 11
$ sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-11 11
```

### 3. Check the available compilers list on your Ubuntu 20.04 system and select desired version by entering relevant selection number: 

#### 3.1 For C and C++ compilers execute:
```shell
$ sudo update-alternatives --config gcc
There are 3 choices for the alternative gcc (providing /usr/bin/gcc).

  Selection    Path            Priority   Status
------------------------------------------------------------
  0            /usr/bin/gcc-9    9         auto mode
  1            /usr/bin/gcc-7    7         manual mode
* 2            /usr/bin/gcc-8    8         manual mode
  3            /usr/bin/gcc-9    9         manual mode
  4            /usr/bin/gcc-11  11         manual mode
Press  to keep the current choice[*], or type selection number:
```

#### 3.2 For C compiler execute:
```shell
$ sudo update-alternatives --config g++
There are 3 choices for the alternative g++ (providing /usr/bin/g++).

  Selection    Path            Priority   Status
------------------------------------------------------------
* 0            /usr/bin/g++-9    9         auto mode
  1            /usr/bin/g++-7    7         manual mode
  2            /usr/bin/g++-8    8         manual mode
  3            /usr/bin/g++-9    9         manual mode
  4            /usr/bin/g++-11  11         manual mode

Press  to keep the current choice[*], or type selection number:
```

### 4. Each time after switch check your currently selected compiler version:
```shell
$ gcc --version
$ g++ --version
```

### 5. Install php-cpp (C++ library):
```shell
$ cd php-cpp
$ make
$ sudo make install
```

### 6. Custom php extension:
#### 6.1 Adapt Makefile:
*./Examples/custom_ext/Makefile*
```Makefile
NAME				=	custom_ext

INI_DIR				=	/etc/php/8.1/mods-available/

...
```

#### 6.1 Install extension:
```shell
$ cd Examples/custom_ext
$ make
$ sudo make install 
```